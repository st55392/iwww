<?php
$message = "";
if (isset($_POST['odber-novinek'])) {
    if (!empty($_POST['email'])) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $message = "You are subscribed";
        } else {
            $message = "Bad formatted email address!";
        }
    } else {
        $message = "Email address is needed!";
    }
}

if (!empty($message)) {
    echo $message;
    $message = "";
}
?>