
<!doctype html>
<html class="no-js" lang="cs">

<head>
  <meta charset="utf-8">
  <title>Tady je Krakonošovo</title>

  <link rel="stylesheet" href="css/layout.css">
</head>

<body>
<header>
  <div id="header-title">Krakonošovo</div>
  <img id="header-img" src="./img/ig_logo.png" alt="logo">
  <nav id="menu">
    <a href="#">Domů</a>
    <a href="#">Blog</a>
    <a href="#">Kontakt</a>
  </nav>
</header>

<section id="hero">
  <div>
    <h1>Jsem váš web designer!</h1>
    <a href="#">
      Objednejte si mě!
    </a>
  </div>
</section>

<main>
  <div class = "center-wrapper">
    <div>
      <h2>Kdo jsem?</h2>
      <p>
        DAfeefede <strong>DDD</strong>, <i>EFEEadd</i> edere, <u>dddd</u>
      </p>
      <hr>
    </div>

    <div class = "flex-wrap">
      <div class = "card">
        <img src="./img/zakaznik1.png" alt="logo zakaznika1">
        <h2>Zákazník 1</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Accusantium, consequuntur dolor, dolore earum et illum inventore labore laboriosam molestiae nam nulla porro
          quae
          quasi quia repellat tempore temporibus voluptates voluptatibus.
        </p>
        <a href="#">
          <div>Více...</div>
        </a>
      </div>

      <div class = "card">
        <img src="./img/zakaznik2.png" alt="logo zakaznika2">
        <h2>Zákazník 2</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Accusantium, consequuntur dolor, dolore earum et illum inventore labore laboriosam molestiae nam nulla porro
          quae
          quasi quia repellat tempore temporibus voluptates voluptatibus.
        </p>
        <a href="#">
          <div>Více...</div>
        </a>
      </div>
      <div class = "card">
        <img src="./img/zakaznik3.png" alt="logo zakaznika3">
        <h2>Zákazník 3</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Accusantium, consequuntur dolor, dolore earum et illum inventore labore laboriosam molestiae nam nulla porro
          quae
          quasi quia repellat tempore temporibus voluptates voluptatibus.
        </p>
        <a href="#">
          <div>Více...</div>
        </a>
      </div>
    </div>
  </div>
  <hr>
  <div>
    <strong>Další zákazníci:</strong>
    <div>
      <img src="./img/price.png" alt="budouci zakaznik">
      <img src="./img/price.png" alt="budouci zakaznik">
      <img src="./img/price.png" alt="budouci zakaznik">
      <img src="./img/price.png" alt="budouci zakaznik">
    </div>
  </div>
</main>

<footer class = "full-width-wrapper">
  <div class = "flex-wrap">
    <section>
      <h4>Něco o mě</h4>
      <ul>
        <li><a href="#">Pracujte se mnou</a></li>
        <li><a href="#">Reference</a></li>
        <li><a href="#">Kontaktujte mě</a></li>
        <li><a href="#">Autoři</a></li>
        <li><a href="#">Login</a></li>
      </ul>
    </section>

    <section>
      <h4>Blog news</h4>
      <ol>
        <li><a href="#">Clanek 1</a></li>
        <li><a href="#">Clanek 2</a></li>
        <li><a href="#">Clanek 3</a></li>
        <li><a href="#">Clanek 4</a></li>
        <li><a href="#">Clanek 5</a></li>
      </ol>
    </section>

    <section>
      <h4>Kontakt</h4>
      <address>
        Krakonošovo <br>
        Krkonošská 1123, Krkonoše<br>
        CZE<br>
        +420 123 456 789<br>
        Email: <a href="mailto:sef@krakonosovo.cz"> sef@krakonosovo.cz</a><br>
      </address>
    </section>

      <?php
      $message = "";
      if (isset($_POST['odber-novinek'])) {
          if (!empty($_POST['email'])) {
              if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                  $message = "You are subscribed";
              } else {
                  $message = "Bad formatted email address!";
              }
          } else {
              $message = "Email address is needed!";
          }
      }
      ?>
    <section id = "footer-newsletter">
      <h4>Odběr novinek</h4>
      <form method="POST" action="?=CURRENT_URL?">
        <div>
          <label>
            Vlož svou emailovou adresu:
          </label>
        </div>
        <div>
          <input type="email" name="email">
        </div>
        <div>
          <input type="submit" name="odber-novinek" value="Odebírat!">
            <?php
            if (!empty($message)) {
                echo $message;
                $message = "";
            }
            ?>
        </div>
      </form>
    </section>

    <section>
      <h4>CR</h4>
      <p>
        Copyright 2000 - 2019 <a href="https://www.google.com">Karlos</a>
      </p>
    </section>
  </div>
</footer>
</body>

</html>
