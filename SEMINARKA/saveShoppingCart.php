<?php
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
try{
    $statement = $conn->prepare("select * from kosiky where idUZIVATELE = ?");
    $statement->execute([$_SESSION["id"]]);
    $data = $statement->fetch(PDO::FETCH_ASSOC);
    $idKos = 0;
    if(empty($data)){
        $statement = $conn->prepare("insert into kosiky(idUZIVATELE)values(?)");
        $statement->execute([$_SESSION["id"]]);
        $idKos = $conn->lastInsertId();
    }else{
        $idKos = $data["idKOSIK"];
        $statement = $conn->prepare("delete from produkty_kosiku where idKOSIK = ?");
        $statement->execute([$idKos]);
    }
    foreach ($_SESSION['cart'] as $cartItemId => $row) {
        $statement = $conn->prepare("insert into produkty_kosiku(idKOSIK,idPRODUKT,pocetKs)values(?,?,?) ");
        $statement->execute([$idKos,$cartItemId,$row["quantity"]]);
    }
    header("Location: index.php?page=shopping_cart");
}catch (PDOException $exception){
    echo  $exception;
}
?>