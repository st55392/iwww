<?php
$message = "";
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $message = '';
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $conn->prepare("select * from uzivatele where email = :email");
    $statement->bindParam(':email', $_POST["email"]);
    $statement->execute();
    $data = $statement->fetch(PDO::FETCH_ASSOC);
    //kontrola vkládaných dat
    if (!empty($data)) {
        if($data["registrovany"]==0){
            $message = 'update';
        }else {
            $message = 'Tento email již je zaregistrován';
        }
    } else if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $message = 'špatný email, ';
    }if($_POST['heslo']!=$_POST['kontrola_hesla']){
        $message = 'hesla se neshodují';
    }

    if($_POST["typ_uzivatele"]=="Admin"){
        $admin = 1;
        $registrovany = 1;
    }else if($_POST["typ_uzivatele"]=="Registrovaný"){
        $admin = 0;
        $registrovany = 1;
    }else{
        $admin = 0;
        $registrovany = 0;
    }

    if($message=='update'){
        $statement = $conn->prepare('update uzivatele set jmeno=:jmeno, prijmeni = :prijmeni, heslo=:heslo,telefon=:telefon,mesto=:mesto,ulice=:ulice,cp=:cp,psc=:psc,admin=:admin,registrovany=:reg where idUZIVATEL = :id');
        $statement->bindParam(":jmeno",$_POST["jmeno"]);
        $statement->bindParam(":prijmeni",$_POST["prijmeni"]);
        $statement->bindParam(":heslo",$_POST["heslo"]);
        $statement->bindParam(":telefon",$_POST["telefon"]);
        $statement->bindParam(":mesto",$_POST["mesto"]);
        $statement->bindParam(":ulice",$_POST["ulice"]);
        $statement->bindParam(":cp",$_POST["cp"]);
        $statement->bindParam(":psc",$_POST["psc"]);
        $statement->bindParam(":admin",$admin);
        $statement->bindParam(":registrovany",$registrovany);
        $statement->bindParam(":id",$data["idUZIVATEL"]);
        $statement->execute();
        $message = "Uživatel vytvořen";
    }else if ($message == '') {
        try {
            $statement = $conn->prepare('insert into uzivatele (email,jmeno,prijmeni,heslo,telefon,mesto,ulice,cp,psc,admin,registrovany) 
                                        values (:email,:jmeno,:prijmeni,:heslo,:telefon,:mesto,:ulice,:cp,:psc,:admin,:registrovany)');
            $statement->bindParam(":email",$_POST["email"]);
            $statement->bindParam(":jmeno",$_POST["jmeno"]);
            $statement->bindParam(":prijmeni",$_POST["prijmeni"]);
            $statement->bindParam(":heslo",$_POST["heslo"]);
            $statement->bindParam(":telefon",$_POST["telefon"]);
            $statement->bindParam(":mesto",$_POST["mesto"]);
            $statement->bindParam(":ulice",$_POST["ulice"]);
            $statement->bindParam(":cp",$_POST["cp"]);
            $statement->bindParam(":psc",$_POST["psc"]);
            $statement->bindParam(":admin",$admin);
            $statement->bindParam(":registrovany",$registrovany);
            $statement->execute();
            $message = "Uživatel vytvořen";

            if ($_SESSION["admin"] == 1) {
                header("Location: index.php?page=users/users");
            } else {
                header("Location: index.php?page=profil");
            }
        } catch (PDOException $exception) {
            echo $exception;
            $message= 'Nepodařilo se vytvořit uživatele';
        }
    }
    echo $message;
}
?>

<section class="form">
    <h1>Nový uživatel</h1>
    <form method="post">
        Email:<br>
        <input type="email" name="email" required="true"><br>
        Telefon:<br>
        <input type="text" name="telefon" required="true"><br>
        Jméno:<br>
        <input type="text" name="jmeno" required="true"><br>
        Příjmení:<br>
        <input type="text" name="prijmeni" required="true"><br>
        Město:<br>
        <input type="text" name="mesto" required="true"><br>
        Ulice:<br>
        <input type="text" name="ulice" required="true"><br>
        Čp:<br>
        <input type="number" name="cp" required="true"><br>
        Psč:<br>
        <input type="text" name="psc" required="true"><br>
        Heslo:<br>
        <input type="password" name="heslo" required="true"><br>
        Kontrola hesla:<br>
        <input type="password" name="kontrola_hesla" required="true"><br>

        <?php
            if(isset($_SESSION["admin"])&&$_SESSION["admin"]==1){
                echo'
                Typ uživatele: <br>
                <select name="typ_uzivatele" required="true">
                    <option>Admin</option>
                    <option>Registrovaný</option>
                    <option>Neregistrovaný</option>
                </select>';
            }else{
                echo'
                <select name="typ_uzivatele" required="true" hidden="true">
                    <option>Registrovaný</option>
                </select>';
            }
        ?>
        <br>
        <input type="submit" name="registrovat" value="Vytvořit">
    </form>
</section>

