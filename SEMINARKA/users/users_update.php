<?php
$message = "";
try {
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        $statement = $conn->prepare("select * from uzivatele where idUZIVATEL = ? ");
        $statement->execute([$_GET["id"]]);
        $data = $statement->fetch(PDO::FETCH_ASSOC);

        echo '<h1>Úprava uživatele</h1>
                <form method="post">
                Email:<br>
                <input type="email" name="email" required="true" value="' . $data["email"] . '"><br>
                Telefon:<br>
                <input type="text" name="telefon" required="true" value="' . $data["telefon"] . '"><br>
                Jméno:<br>
                <input type="text" name="jmeno" required="true" value="' . $data["jmeno"] . '"><br>
                Příjmení:<br>
                <input type="text" name="prijmeni" required="true" value="' . $data["prijmeni"] . '"><br>
                Město:<br>
                <input type="text" name="mesto" required="true" value="' . $data["mesto"] . '"><br>
                Ulice:<br>
                <input type="text" name="ulice" required="true" value="' . $data["ulice"] . '"><br>
                Čp:<br>
                <input type="number" name="cp" required="true" value="' . $data["cp"] . '"><br>
                Psč:<br>
                <input type="text" name="psc" required="true" value="' . $data["psc"] . '"><br>
                Heslo:<br>
                <input type="password" name="heslo" required="true" value="' . $data["heslo"] . '"><br>';
        if ($_SESSION["admin"] == 1) {
            echo 'Typ uživatele: <br>
                    <select name = "typ_uzivatele" required = "true">
                        <option> Admin</option>
                        <option> Registrovaný</option>
                        <option> Neregistrovaný</option>
                    </select>
                    <br >';
        } else {
            echo '
                    <select name = "typ_uzivatele" required = "true" hidden="true">
                        <option>Registrovaný</option>
                    </select>
                    <br>';
        }
        echo '<input type="submit" name="registrovat" value="Upravit">
            </form>';

    } else if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $message = '';
        $statement = $conn->prepare("select * from uzivatele where email = :email");
        $statement->bindParam(':email', $_POST["email"]);
        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
        //kontrola vkládaných dat

        if (count($data) > 1) {
            $message = 'Tento email již je zaregistrován';
        } else if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $message = 'špatný email';
        }
        if ($_POST["typ_uzivatele"] == "Admin") {
            $admin = 1;
            $registrovany = 1;
        } else if ($_POST["typ_uzivatele"] == "Registrovaný") {
            $admin = 0;
            $registrovany = 1;
        } else {
            $admin = 0;
            $registrovany = 0;
        }
        if ($message == '') {
            try {

                $statement = $conn->prepare('update uzivatele set email=:email, jmeno=:jmeno, prijmeni = :prijmeni, heslo=:heslo,telefon=:telefon,mesto=:mesto,ulice=:ulice,cp=:cp,psc=:psc,admin=:admin,registrovany=:reg where idUZIVATEL = :id');
                $statement->bindParam(":email", $_POST["email"]);
                $statement->bindParam(":jmeno", $_POST["jmeno"]);
                $statement->bindParam(":prijmeni", $_POST["prijmeni"]);
                $statement->bindParam(":heslo", $_POST["heslo"]);
                $statement->bindParam(":telefon", $_POST["telefon"]);
                $statement->bindParam(":mesto", $_POST["mesto"]);
                $statement->bindParam(":ulice", $_POST["ulice"]);
                $statement->bindParam(":cp", $_POST["cp"]);
                $statement->bindParam(":psc", $_POST["psc"]);
                $statement->bindParam(":admin", $admin);
                $statement->bindParam(":reg", $registrovany);
                $statement->bindParam(":id", $_GET["id"]);
                $statement->execute();


                if ($_SESSION["admin"] == 1) {
                    header("Location: index.php?page=users/users");
                } else {
                    header("Location: index.php?page=profil");
                }
            } catch (PDOException $exception) {
                echo $exception;
                $message = 'Nepodařilo se vytvořit uživatele';
            }
        }
        echo $message;
    }
} catch (PDOException $ex) {
    echo $ex;
    $message = "Neprobehlo";
}
echo $message;
?>



