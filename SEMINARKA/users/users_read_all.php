<h1>Uživatelé</h1>
<?php
try {
    echo "<a href = \"?page=users/users&action=new\">Vytvoř uživatele</a><p></p>";
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = $conn->query("select * from uzivatele")->fetchAll();
    echo "<table style='width: 100%' border='1'>
            <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Jméno</th>
                <th>Příjmení</th>
                <th>Telefon</th>
                <th>Admin</th>
                <th>Registrovany</th>
                <th>Adresa</th>
                <th>U</th><th>S</th></tr>";
    foreach($data as $row){
        $admin = "ne";
        $registrovany = "ne";
        if($row["admin"]==1){
            $admin = "ano";
        }if($row["registrovany"]==1){
            $registrovany = "ano";
        }
        echo'<tr>
            <td>'.$row["idUZIVATEL"].'</td>
            <td>'.$row["email"].'</td>
            <td>'.$row["jmeno"].'</td>
            <td>'.$row["prijmeni"].'</td>
            <td>'.$row["telefon"].'</td>
            <td>'.$admin.'</td>
            <td>'.$registrovany.'</td>
            <td>'.$row["ulice"].' '.$row["cp"].', '.$row["psc"].' '.$row["mesto"].'</td>
            <td><a href="?page=users/users&action=update&id='.$row["idUZIVATEL"].'">Upravit</a></td>
            <td><a href="?page=users/users&action=delete&id='.$row["idUZIVATEL"].'">Smazat</td>
            </tr>';
    }
    echo'</table>';

}
catch(PDOException $ex){
    echo 'nepodařilo se načíst uživatele';
}

?>
<p></p>
