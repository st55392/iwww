<?php
$message = "";
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    try {
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $conn->prepare("delete from uzivatele where idUZIVATEL = ?");
        $statement->execute([$_GET['id']]);
        $message = "Uživatel odstraněn";
    } catch (PDOException $ex) {
        $message = "Neprobehlo";
    }

}
echo $message;
?>
