<?php
$conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $uploaddir = './uploads/';
    $uploadfile = $uploaddir . basename($_FILES['jsonFile']['name']);
    $extension = array("json", "JSON");
    $UploadOk = true;
    $ext = pathinfo($_FILES["jsonFile"]["name"], PATHINFO_EXTENSION);
    if (in_array($ext, $extension) == false) {
        $UploadOk = false;
        echo "Soubor se nepodařilo nahrát.";
    }
    if ($UploadOk == true) {
        if (move_uploaded_file($_FILES['jsonFile']['tmp_name'], $uploadfile)) {
            $jsondata = file_get_contents($uploadfile);
            $obj = json_decode($jsondata, true);
            echo "Soubor se úspěšně nahrál.";
        }
    }
    echo count($obj);
    for ($i = 1; $i <= count($obj); $i++) {
        echo "Počet položek: " . $obj[$i][$i]["pocetProduktu"] . "<br>";
        $stav = $obj[$i][$i]["stav"];
        $statement = $conn->prepare("select * from stav_objednavek where stav = ?");
        $statement->execute([$stav]);
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        $idStav = $row["idSTAV"];
        echo "Stav: " . $stav . "<br>";
        $email = $obj[$i][$i]["email"];
        echo "Email: " . $email . "<br>";

        $statement = $conn->prepare("select idUZIVATEL from uzivatele where email = :email");
        $statement->bindParam(":email", $email);
        $statement->execute();
        $row = $statement->fetch(PDO::FETCH_ASSOC);
        $idUz = $row["idUZIVATEL"];

        $statement = $conn->prepare("insert into objednavky(cas_datum_vytvoreni,idUZIVATELE,idSTAV)values(Now(),:idUz,:idStav)");
        $statement->bindParam(":idStav",$idStav);
        $statement->bindParam(":idUz",$idUz);
        $statement->execute();
        $idObj = $conn->lastInsertId();

        for ($j = 1; $j <= $obj[$i][$i]["pocetProduktu"]; $j++) {
            $nazev = $obj[$i][$i][$j]["nazev"];
            $cena = $obj[$i][$i][$j]["cena"];
            $popis = $obj[$i][$i][$j]["popis"];
            $pocetKs = $obj[$i][$i][$j]["pocetKs"];
            $stupnivost = $obj[$i][$i][$j]["stupnivost"];
            $alkohol = $obj[$i][$i][$j]["alkohol"];
            $druh = $obj[$i][$i][$j]["druh"];
            $vyrobceNazev = $obj[$i][$i][$j]["vyrobce"];


            $statement = $conn->prepare("select idVYROBCE from vyrobci where nazev = :nazev");
            $statement->bindParam(":nazev", $vyrobceNazev);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            $idVyr = $row["idVYROBCE"];

            $statement = $conn->prepare("select idPRODUKT from produkty where nazev = :nazev and popis = :popis and stupnivost = :stupnivost and alkohol = :alkohol and druh = :druh and idVYROBCE = :idVyr");
            $statement->bindParam(":nazev", $nazev);
            $statement->bindParam(":popis", $popis);
            $statement->bindParam(":stupnivost", $stupnivost);
            $statement->bindParam(":alkohol", $alkohol);
            $statement->bindParam(":druh", $druh);
            $statement->bindParam(":idVyr", $idVyr);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            $idProd = $row["idPRODUKT"];

            $statement = $conn->prepare("insert into produkty_objednavek(idPRODUKT,idOBJEDNAVKA,pocetKs,cena)values(:idProd,:idObj,:pocetKs,:cena)");
            $statement->bindParam(":idProd", $idProd);
            $statement->bindParam(":idObj", $idObj);
            $statement->bindParam(":pocetKs", $pocetKs);
            $statement->bindParam(":cena",$cena);
            $statement->execute();
        }
    }
}

?>
<form method="post" enctype="multipart/form-data">
    <input type="file" name="jsonFile" accept=".json">
    <input type="submit" value="Import" name="buttonImport">
</form>
