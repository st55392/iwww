<h1>Nákupní košík</h1>
<hr>
<?php
if(isset($_SESSION["cart"])) {
    try {
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $celkovaCena = 0;
        foreach ($_SESSION['cart'] as $cartItemId => $row) {
            $statementVyrobek = $conn->prepare("select * from produkty where idPRODUKT = :idProd");
            $statementVyrobek->bindParam(":idProd", $cartItemId);
            $statementVyrobek->execute();
            $dataVyrobek = $statementVyrobek->fetch(PDO::FETCH_ASSOC);
            echo '
            <div class="basket_item">
            <h2>' . $dataVyrobek["nazev"] . '</h2>
            <img src="readImg.php?idPRODUKTY=' . $dataVyrobek["idPRODUKT"] . '" alt="PIVO" height =100px>
            Cena za jednotku: ' . $dataVyrobek["cena"] . ' Kč, Počet kusů: 
                ' . $row["quantity"] . '
                
                    <a class ="btn_cart" href = "index.php?page=addToShoppingCart&action=plus&id=' . $dataVyrobek["idPRODUKT"] . '">+</a>
                    <a class ="btn_cart" href = "index.php?page=addToShoppingCart&action=minus&id=' . $dataVyrobek["idPRODUKT"] . '">-</a>
                    <a class ="btn_cart" href = "index.php?page=addToShoppingCart&action=del&id=' . $dataVyrobek["idPRODUKT"] . '">X</a>
                
                , Cena celkem: ' . $row["quantity"] * $dataVyrobek["cena"] . ' Kč 
                
            </div>
            <hr>
        ';
            $celkovaCena += $dataVyrobek["cena"] * $row["quantity"];
        }
        echo '
            <p><strong>Celková cena: ' . $celkovaCena . ' Kč </strong>+ doprava</p>
            <div class="card">
                <a href = "' . BASE_URL . '?page=objednavky/objednej">OBJEDNEJ</a>';
        if (isset($_SESSION["id"])) {
            echo '<p></p><a href = "' . BASE_URL . '?page=saveShoppingCart">Uložit košík</a>';
        }
        echo '</div>';


    } catch (PDOException $exception) {
        echo $exception;
    }
}else{
    echo"Žádné produkty v košíku<p></p>";
}

?>

