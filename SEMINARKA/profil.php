<?php
$conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$statement = $conn->prepare("select * from uzivatele where idUZIVATEL = :id");
$statement->bindParam(':id', $_SESSION['id']);
$statement->execute();
$data = $statement->fetch(PDO::FETCH_ASSOC);


if(!empty($data)){
    echo'<h1>Můj profil:</h1>';
    echo'Email: '.$data['email'].'<br>';
    echo 'Jmeno a prijmeni: '.$data['jmeno'].' '.$data['prijmeni'].'<br>';
    echo 'Adresa: '.$data['ulice'].' '.$data['cp'].', '.$data['psc'].' '.$data['mesto'].'<br>';
    echo '<div class="card"><a href="' . BASE_URL . '?page=users/users&action=update&id='.$_SESSION['id'].'">Změnit údaje</a></div>';

}

?>