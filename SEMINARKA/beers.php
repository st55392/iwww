<h1>Katalog piv</h1>
<?php
$conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dataVyrobci = $conn->query("select * from vyrobci");
$dataDruh = $conn->query("select distinct druh from produkty");
$dataStupnivost = $conn->query("select distinct stupnivost from produkty");

echo '<form method="post">Výrobci: <select name="vyrobce"><option>----</option>';
foreach ($dataVyrobci as $row) {
    echo '<option>' . $row["nazev"] . '</option>';
}
echo '</select> Stupnivost: <select name="stupnivost"><option>----</option>';
foreach ($dataStupnivost as $row) {
    echo '<option>' . $row["stupnivost"] . '</option>';
}
echo '</select>';
echo '</select> Druh: <select name="druh"><option>----</option>';
foreach ($dataDruh as $row) {
    echo '<option>' . $row["druh"] . '</option>';
}
echo '</select>';
echo ' <input type="submit" name = "filtrovat" value="Filtrovat"></form><p></p> ';

echo '<form method="post">Hledání: <input type="text" name="search"> <input type="submit" name="hledat" value="Hledat"></form>';

if(isset($_POST["hledat"])){
   $sr = "'%".$_POST["search"]."%'";
   $statement = $conn->prepare("select * from produkty where nazev like".$sr."  or popis like".$sr);
   $statement->execute();
   $data = $statement->fetchAll(PDO::FETCH_ASSOC);
}
else if (isset($_POST["filtrovat"])) {
    $stm = "select * from produkty ";
    $filtr = array(["", "", ""]);
    $pocetFiltru = 0;
    $exec = array();
    if ($_POST["vyrobce"] != "----") {
        $statementVyrobce = $conn->prepare("select * from vyrobci where nazev = ?");
        $statementVyrobce->execute([$_POST["vyrobce"]]);
        $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);
        $filtr[1] = $dataVyrobce["idVYROBCE"];
        $pocetFiltru++;
    }
    if ($_POST["stupnivost"] != "----") {
        $filtr[2] = $_POST["stupnivost"];
        $pocetFiltru++;
    }
    if ($_POST["druh"] != "----") {
        $filtr[3] = $_POST["druh"];
        $pocetFiltru++;
    }
    switch ($pocetFiltru) {
        case 3:
            $stm = $stm . "where ";
            $stm = $stm . "idVYROBCE = ? and stupnivost = ? and druh = ?";
            $exec[1] = $filtr[1];
            $exec[2] = $filtr[2];
            $exec[3] = $filtr[3];
            break;
        case 2:
            $stm = $stm . "where ";
            if (!empty($filtr[1]) && $filtr[1] != "") {
                $stm = $stm . " idVYROBCE = ? and ";
                $exec[1] = $filtr[1];
                $pocetFiltru--;
            }
            if (!empty($filtr[2]) && $filtr[2] != "" && $pocetFiltru == 2) {
                $stm = $stm . " stupnivost =  ? and ";
                $exec[1] = $filtr[2];
            } else if (!empty($filtr[2]) && $filtr[2] != "" && $pocetFiltru == 1) {
                $stm = $stm . " stupnivost = ?";
                $exec[2] = $filtr[2];
            }
            if (!empty($filtr[3]) && $filtr[3] != "") {
                $stm = $stm . " druh = ?";
                $exec[2] = $filtr[3];
            }
            break;
        case 1:
            $stm = $stm . "where ";
            if (!empty($filtr[1]) && $filtr[1] != "") {
                $stm = $stm . " idVYROBCE = ?";
                $exec[1]=$filtr[1];
            } else if (!empty($filtr[2]) && $filtr[2] != "") {
                $stm = $stm . " stupnivost = ?";
                $exec[1]=$filtr[2];
            } else if (!empty($filtr[3]) && $filtr[3] != "") {
                $stm = $stm . " druh = ?";
                $exec[1]=$filtr[3];
            }
            break;
        case 0:
            break;
    }

    $statementVyrobky = $conn->prepare($stm);
    switch (sizeof($exec)){
        case 3:
            $statementVyrobky->execute([$exec[1],$exec[2],$exec[3]]);
            echo $exec[1];
            echo $exec[2];
            echo $exec[3];
            break;
        case 2:
            echo $exec[1];
            echo $exec[2];
            $statementVyrobky->execute([$exec[1],$exec[2]]);

            break;
        case 1:
            $statementVyrobky->execute([$exec[1]]);
            break;
        case 0:
            $statementVyrobky->execute();
            break;
    }
    $data = $statementVyrobky->fetchAll(PDO::FETCH_ASSOC);
} else {
    $data = $conn->query("select * from produkty")->fetchAll(PDO::FETCH_ASSOC);
}
echo '<div class="flex-wrap">';
foreach ($data as $row) {
    $statementVyrobce = $conn->prepare("select * from vyrobci where idVYROBCE = :idVyrobce");
    $statementVyrobce->bindParam(":idVyrobce", $row["idVYROBCE"]);
    $statementVyrobce->execute();
    $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);
    if (empty($_SESSION["id"])) {
        $_SESSION["id"] = 0;
    }
    echo '<div class="card">
        <img src="readImg.php?idPRODUKTY=' . $row['idPRODUKT'] . '" alt="PIVO">
        <h2>' . $row['nazev'] . '</h2>
        <p>' .
        $row['popis'] . '
        </p>
        <p><strong>Stupnivost: </strong>' . $row["stupnivost"] . '°</p>
        <p><strong>Druh: </strong>' . $row["druh"] . '</p>
        <p><strong>Obsah alkoholu: </strong>' . $row["alkohol"] . ' %</p>
        <p><strong>CENA: </strong>' . $row['cena'] . ' Kč</p>
        <p><strong>Výrobce: </strong>' . $dataVyrobce['nazev'] . '</p>
        <a href="addToShoppingCart.php?action=add&id=' . $row['idPRODUKT'] . '">
            <div>PŘIDAT DO KOŠÍKU</div>
        </a>
    </div>';
}
?>
</div>



