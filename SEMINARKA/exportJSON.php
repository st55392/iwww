<?php
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pole = array();
$radek = 1;
$sloupec = 1;


if($_SESSION["admin"]==1){
    $select_query = "select * from objednavky";
    $statement = $conn->query($select_query);
    while($row=$statement->fetch(PDO::FETCH_ASSOC)){
        $statementUzivatel = $conn->prepare("select * from uzivatele where idUZIVATEL = ?");
        $statementUzivatel->execute([$row["idUZIVATELE"]]);
        $dataUzivatel = $statementUzivatel->fetch(PDO::FETCH_ASSOC);
        $statementPocetVyr = $conn->prepare("select count(*) as pocet from produkty_objednavek where idOBJEDNAVKA = ?");
        $statementPocetVyr->execute([$row["idOBJEDNAVKA"]]);
        $dataPocet = $statementPocetVyr->fetch(PDO::FETCH_ASSOC);
        $statementStav = $conn->prepare("select stav from stav_objednavek where idSTAV = ?");
        $statementStav->execute([$row["idSTAV"]]);
        $dataStav = $statementStav->fetch(PDO::FETCH_ASSOC);
        $pole[$radek][$sloupec]["pocetProduktu"]=$dataPocet["pocet"];
        $pole[$radek][$sloupec]["stav"]=$dataStav["stav"];
        $pole[$radek][$sloupec]["email"]=$dataUzivatel["email"];

        $radekPom = 1;

        $statementPom = $conn->prepare("select * from produkty_objednavek where idOBJEDNAVKA = ?");
        $statementPom->execute([$row["idOBJEDNAVKA"]]);
        while($rowPom = $statementPom->fetch(PDO::FETCH_ASSOC)){
            $statementProdukt = $conn->prepare("select * from produkty where idPRODUKT = ?");
            $statementProdukt->execute([$rowPom["idPRODUKT"]]);
            $dataProdukt = $statementProdukt->fetch(PDO::FETCH_ASSOC);
            $statementVyrobce =$conn->prepare("select nazev from vyrobci where idVYROBCE = ?");
            $statementVyrobce->execute([$dataProdukt["idVYROBCE"]]);
            $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);
            $pole[$radek][$sloupec][$radekPom]["nazev"] = $dataProdukt["nazev"];
            $pole[$radek][$sloupec][$radekPom]["cena"] = $rowPom["cena"];
            $pole[$radek][$sloupec][$radekPom]["popis"] = $dataProdukt["popis"];
            $pole[$radek][$sloupec][$radekPom]["stupnivost"] = $dataProdukt["stupnivost"];
            $pole[$radek][$sloupec][$radekPom]["alkohol"] = $dataProdukt["alkohol"];
            $pole[$radek][$sloupec][$radekPom]["druh"] = $dataProdukt["druh"];
            $pole[$radek][$sloupec][$radekPom]["pocetKs"] = $rowPom["pocetKs"];
            $pole[$radek][$sloupec][$radekPom++]["vyrobce"] = $dataVyrobce["nazev"];
        }
        $sloupec++;
        $radek++;
    }
    $pole1 = json_encode($pole,JSON_UNESCAPED_UNICODE);
    $adresa = './uploads/exportObj.json';
    file_put_contents($adresa,$pole1);
    if (file_exists($adresa)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($adresa));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($adresa));
        ob_clean();
        flush();
        readfile($adresa);
        unlink($adresa);
    }
}
?>
