<?php
include "config.php";
if(isset($_GET['idPRODUKTY'])){
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $conn->prepare("select obrazek, nazev_obrazek,typ_obrazek from produkty where idPRODUKT = :idProd");
    $statement->bindParam(":idProd",$_GET['idPRODUKTY']);
    $statement->execute();
    $data = $statement->fetch(PDO::FETCH_ASSOC);
    file_put_contents($data["nazev_obrazek"],$data["obrazek"]);
    header("Content-type: ". $data["typ_obrazek"]);
    echo $data['obrazek'];
}

?>
