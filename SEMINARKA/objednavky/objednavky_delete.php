<?php
$message = "";
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    try {
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statementAdr = $conn->prepare("delete from objednavky where idOBJEDNAVKA = ?");
        $statementAdr->execute([$_GET['idObj']]);
        $message = "Objednávka odstraněna ";
    } catch (PDOException $ex) {
        $message = "Neprobehlo";
    }

}
echo $message;
?>