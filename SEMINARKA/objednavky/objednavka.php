<?php
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if (!empty($_GET["idObj"])) {
    if(!empty($_POST["stav"])){
        $statementSelStav = $conn->prepare("select * from stav_objednavek where stav = ?");
        $statementSelStav->execute([$_POST["stav"]]);
        $dataStavu = $statementSelStav->fetch(PDO::FETCH_ASSOC);
        $statementUpdStav = $conn->prepare("update objednavky set idSTAV = ? where idOBJEDNAVKA = ?");
        $statementUpdStav->execute([$dataStavu["idSTAV"],$_GET["idObj"]]);
    }
    $statementObj = $conn->prepare("select * from produkty_objednavek where idOBJEDNAVKA = :idObj");
    $statementObj->bindParam(":idObj", $_GET["idObj"]);
    $statementObj->execute();
    $celkovaCena = 0;
    $dataProduktyObjednavky = $statementObj->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($dataProduktyObjednavky)) {
        $statementObjednavka = $conn->prepare("select * from objednavky where idOBJEDNAVKA = ?");
        $statementObjednavka->execute([$_GET["idObj"]]);
        $dataObjednavka = $statementObjednavka->fetch(PDO::FETCH_ASSOC);
        echo '<h1>Objednávka č. ' . $_GET["idObj"] . '</h1>';
        $statementStav = $conn->prepare("select stav from stav_objednavek where idSTAV = ?;");
        $statementStav->execute([$dataObjednavka["idSTAV"]]);
        $dataStav = $statementStav->fetch(PDO::FETCH_ASSOC);
        echo'Stav objednávky: '.$dataStav["stav"];
        if(isset($_SESSION["admin"])&&$_SESSION["admin"]==1){
            $statementStavy = $conn->prepare("select stav from stav_objednavek");
            $statementStavy->execute();
            $dataStavy = $statementStavy->fetchAll(PDO::FETCH_ASSOC);

            echo '<form method="post">';
            echo'<select name = "stav">';
            foreach ($dataStavy as $row) {
                echo'<option>' . $row['stav'] . '</option>';
            }
            echo'</select>';
            echo '<input type="submit" value="Změnit stav">
                </form>';
        }
        foreach ($dataProduktyObjednavky as $row) {
            $statementVyrobek = $conn->prepare("select * from produkty where idPRODUKT = :idProd");
            $statementVyrobek->bindParam(":idProd", $row["idPRODUKT"]);
            $statementVyrobek->execute();
            $dataVyrobek = $statementVyrobek->fetch(PDO::FETCH_ASSOC);
            echo '
            <div class="center-wrap">
            <h2>' . $dataVyrobek["nazev"] . '</h2>
            <img src="readImg.php?idPRODUKTY=' . $dataVyrobek["idPRODUKT"] . '" alt="PIVO" width =100px>
            Cena za jednotku: ' . $row["cena"] . ' Kč, Počet kusů: ' . $row["pocetKs"] . '
                          
                , Cena celkem: ' . $row["pocetKs"] * $row["cena"] . ' Kč 
            </div>
            <hr>
        ';
            $celkovaCena += $row["cena"] * $row["pocetKs"];
        }
        echo 'Celková cena: '.$celkovaCena.' Kč<p></p>';
    }
} else {
    header("Location: index.php");
}
?>