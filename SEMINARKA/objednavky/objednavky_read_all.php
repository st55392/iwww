<h1>Objednávky</h1>
<div class="full-width-wrapper">
<?php
$conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ($_SESSION["admin"] == 1) {
    echo "<a href=\"?page=exportJSON\">EXPORTOVAT JSON</a><p></p>";
    echo "<a href=\"?page=importJSON\">IMPORTOVAT JSON</a><p></p>";
}
echo "<table style='width: 100%' border='1'>
            <tr>
                <th>ID</th>
                <th>Cas a datum vytvoreni</th>
                <th>Stav</th>
                <th>Uživatel</th>
                <th>O</th>";
if ($_SESSION["admin"] == 1) {
    echo "<th>D</th>";
}
echo "</tr>";
if ($_SESSION["admin"] == 1) {
    $statementObj = $conn->prepare("select * from objednavky");
    $statementObj->execute();
} else {
    $statementObj = $conn->prepare("select * from objednavky where idUZIVATELE = ?");
    $statementObj->execute([$_SESSION["id"]]);
}
$data = $statementObj->fetchAll(PDO::FETCH_ASSOC);
foreach ($data as $row) {
    $statementStav = $conn->prepare("select stav from stav_objednavek where idSTAV = ?");
    $statementStav->execute([$row["idSTAV"]]);
    $dataStav = $statementStav->fetch(PDO::FETCH_ASSOC);
    $statementUzivatel = $conn->prepare("select * from uzivatele where idUZIVATEL = ?");
    $statementUzivatel->execute([$row["idUZIVATELE"]]);
    $dataUzivatel = $statementUzivatel->fetch(PDO::FETCH_ASSOC);
    echo '<tr>
                <td>' . $row["idOBJEDNAVKA"] . '</td>
                <td>' . $row["cas_datum_vytvoreni"] . '</td>
                <td>' . $dataStav["stav"] . '</td>
                <td>' . $dataUzivatel["jmeno"] . ' ' . $dataUzivatel["prijmeni"] . '</td>
                <td><a href="?page=objednavky/objednavky&action=open&idObj=' . $row["idOBJEDNAVKA"] . '">Zobrazit</a></td>';
    if ($_SESSION["admin"] == 1) {
        echo '       <td><a href="?page=objednavky/objednavky&action=delete&idObj=' . $row["idOBJEDNAVKA"] . '">Vymazat</a></td>
       ';
    }
    echo '</tr>';
}
echo'</table>';
?></div>
<p></p>
