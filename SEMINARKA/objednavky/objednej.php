<?php
$conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if (empty($_SESSION["id"])) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $statementUzi = $conn->prepare("select idUZIVATEL from uzivatele where email = ?");
        $statementUzi->execute([$_POST["email"]]);
        $idUz = $statementUzi->fetch(PDO::FETCH_ASSOC);
        if (empty($idUz)) {
            $statement = $conn->prepare('insert into uzivatele (email,jmeno,prijmeni,heslo,telefon,mesto,ulice,cp,psc,admin,registrovany) 
                                        values (:email,:jmeno,:prijmeni,\'heslo\',:telefon,:mesto,:ulice,:cp,:psc,0,0)');
            $statement->bindParam(":email",$_POST["email"]);
            $statement->bindParam(":jmeno",$_POST["jmeno"]);
            $statement->bindParam(":prijmeni",$_POST["prijmeni"]);
            $statement->bindParam(":telefon",$_POST["telefon"]);
            $statement->bindParam(":mesto",$_POST["mesto"]);
            $statement->bindParam(":ulice",$_POST["ulice"]);
            $statement->bindParam(":cp",$_POST["cp"]);
            $statement->bindParam(":psc",$_POST["psc"]);
            $statement->execute();
            $idUz["idUZIVATEL"] = $conn->lastInsertId();
        }else{
            $statement = $conn->prepare('update uzivatele set jmeno=:jmeno, prijmeni = :prijmeni, telefon=:telefon,mesto=:mesto,ulice=:ulice,cp=:cp,psc=:psc where idUZIVATEL = :id');
            $statement->bindParam(":jmeno",$_POST["jmeno"]);
            $statement->bindParam(":prijmeni",$_POST["prijmeni"]);
            $statement->bindParam(":telefon",$_POST["telefon"]);
            $statement->bindParam(":mesto",$_POST["mesto"]);
            $statement->bindParam(":ulice",$_POST["ulice"]);
            $statement->bindParam(":cp",$_POST["cp"]);
            $statement->bindParam(":psc",$_POST["psc"]);
            $statement->bindParam(":id",$idUz["idUZIVATEL"]);
            $statement->execute();
        }
    } else {
        echo '
            <section id="registrate">
            <h1>Vaše údaje</h1>
            <form method="post">
                Email:<br>
                <input type="email" name="email" required="true"><br>
                Telefon:<br>
                <input type="text"name="telefon" required="true"><br>
                Jméno:<br>
                <input type="text" name = "jmeno" required="true"><br>
                Příjmení:<br>
                <input type="text" name="prijmeni" required="true"><br>
                Město:<br>
                <input type="text" name="mesto" required="true"><br>
                Ulice:<br>
                <input type="text" name="ulice" required="true"><br>
                Čp:<br>
                <input type="number" name="cp" required="true"><br>
                Psč:<br>
                <input  type="text" name="psc" required="true"><br>
                <input type="submit" name ="registrovat" value="ODESLAT OBJEDNÁVKU">
            </form>
        </section>';
    }
} else {
    $idUz["idUZIVATEL"] = $_SESSION["id"];
}
if(!empty($idUz["idUZIVATEL"])) {
    $statementObj = $conn->prepare("insert into objednavky(cas_datum_vytvoreni,idSTAV,idUZIVATELE) values(Now(),1,?)");
    $statementObj->execute([$idUz["idUZIVATEL"]]);
    $idObj = $conn->lastInsertId();
    foreach ($_SESSION['cart'] as $cartItemId => $row) {
        $statementVyrobek = $conn->prepare("select * from produkty where idPRODUKT = ?");
        $statementVyrobek->execute([$cartItemId]);
        $dataVyrobek = $statementVyrobek->fetch(PDO::FETCH_ASSOC);
        $statementInsDoObj = $conn->prepare("insert into produkty_objednavek(idOBJEDNAVKA,idPRODUKT,pocetKs,cena) values(?,?,?,?)");
        $statementInsDoObj->execute([$idObj, $cartItemId, $row["quantity"],$dataVyrobek["cena"]]);
    }
    unset($_SESSION["cart"]);
    $_GET["idObj"] = $idObj;
    include "objednavky/objednavka.php";
}
?>