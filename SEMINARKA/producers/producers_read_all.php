
<h1>Výrobci</h1>
<?php
try {
    echo "<a href = \"? page=/producers/producers&action=new\">Vytvořit výrobce</a><p></p>";
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = $conn->query("select * from vyrobci")->fetchAll();
    echo "<table style='width: 100%' border='1'>
            <tr>
                <th>ID</th>
                <th>Název</th>
                <th>U</th><th>D</th></tr>";
    foreach($data as $row){
        echo'<tr>
            <td>'.$row["idVYROBCE"].'</td>
            <td>'.$row["nazev"].'</td>
            <td><a href="?page=/producers/producers&action=update&id='.$row["idVYROBCE"].'">Upravit</a></td>
            <td><a href="?page=/producers/producers&action=delete&id='.$row["idVYROBCE"].'"> smazat</td>
            </tr>';
    }
    echo'</table><p></p>';

}
catch(PDOException $ex){
    echo 'Nepodařilo se načíst výrobky';
}

?>
