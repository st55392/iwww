<?php
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    try {
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $statement = $conn->prepare("delete from vyrobci where idVYROBCE = ?");
        $statement->execute([$_GET['id']]);
    }catch (PDOException $exception){
        echo 'Nepodařilo se odstranit';
    }
}
?>
