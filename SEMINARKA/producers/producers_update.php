<?php
try{
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statementVyrobce = $conn->prepare("select * from vyrobci where idVYROBCE = ?");
    $statementVyrobce->execute([$_GET['id']]);
    $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);

    if($_SERVER["REQUEST_METHOD"] === "GET"){
        echo '<h1>Úprava výrobce</h1>
            <form method="post">
                Název:<br>
                <input type="text" name="nazev" required="true" value ="'.$dataVyrobce['nazev'].'"><br>
                <br>
                <input type="submit" name="upravit" value="Uložit změny">
                <p></p>
            </form>';
    }if($_SERVER["REQUEST_METHOD"] === "POST"){
        $statementUpdate = $conn->prepare("update vyrobci set nazev = ? where idVYROBCE = ?");
        $statementUpdate->execute([$_POST['nazev'],$_GET['id']]);
        header("Location: index.php?page=producers/producers");
    }
}catch (PDOException $exception){

}
?>

