<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $message = '';
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    try {
        $statementVyrobce = $conn->prepare("insert into vyrobci(nazev) values(?)");
        $statementVyrobce->execute([$_POST["nazev"]]);
        header("Location: index.php?page=producers/producers");
    } catch (PDOException $exception) {
        echo $exception;
        $message = 'Nepodařilo se přidat výrobce';
    }
    echo $message;
}
?>

<h1>Nový výrobce</h1>
<form method="post">
    Název:<br>
    <input type="text" name="nazev" required="true"><br>
    <br>
    <input type="submit" name="vytvorit" value="Vytvořit">
</form>
