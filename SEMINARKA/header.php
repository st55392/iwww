<nav id="menu">
    <a href="<?=BASE_URL ?>">Domů</a>
    <a href="<?=BASE_URL ."?page=beers" ?>">Katalog piv</a>
    <a href="<?=BASE_URL ."?page=shopping_cart" ?>">Nákupní košík</a>

    <?php
    if(!empty($_SESSION['id'])){
        echo '<a href="'.BASE_URL .'?page=logout" ?>Odhlásit</a>';
        echo '<a href="'.BASE_URL .'?page=profil" ?>Profil</a>';
        echo '<a href="'.BASE_URL .'?page=objednavky/objednavky" ?>Objednávky</a>';
        if($_SESSION['admin']==1){
            echo '<a href ="'.BASE_URL .'?page=users/users" ?>Uživatelé</a>';
            echo '<a href ="'.BASE_URL .'?page=items/items" ?>Výrobky</a>';
            echo '<a href ="'.BASE_URL .'?page=producers/producers" ?>Výrobci</a>';
            echo '<a href ="'.BASE_URL .'?page=stavy_objednavek/stavy_objednavek" ?>Stavy objednavek</a>';
        }
    }else{
        echo '<a href="'.BASE_URL .'?page=login" ?>Přihlášení</a>';
        echo '<a href = "?page=users/users&action=new">Registrace</a>';
    }
    ?>
</nav>