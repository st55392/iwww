<?php
include "config.php";
?>
<!doctype html>
<html class="no-js" lang="cs">

<head>
    <meta charset="utf-8">
    <title>Beer shop</title>

    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/print.css" type="text/css" media="print">
    <link rel="stylesheet" href="css/respontive.css" type="text/css" media="screen">
</head>

<body>
<section id="hero">
    <div>

    </div>
</section>
<header>
    <div id="header-title">Karlos - beer shop</div>
    <a href="<?= BASE_URL ?>"><img id="header-img" src="./img/pivo.png" alt="logo">
    </a>
    <?php
    include "header.php";
    ?>
</header>
<main>
    <?php
    if (!empty($_GET["page"])) {
        $file = "./" . $_GET["page"] . ".php";
        if (file_exists($file)) {
            include $file;
        } else {
            include "default.php";
        }
    } else {
        include "default.php";
    }
    ?>
</main>
<?php
include "footer.php";
?>

</body>

</html>