<?php
    if($_SERVER["REQUEST_METHOD"]==="POST"){
        $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $conn->prepare("insert into stav_objednavek(stav) values(?)");
        $statement->execute([$_POST["stav"]]);
        header("Location: index.php?page=stavy_objednavek/stavy_objednavek");
    }

?>
<section class="form">
<form method = "post">
    <h1>Přidání stavu objednávky</h1>
    <label for="stav">Stav: </label><br>
    <input type="text" name="stav"><br>
    <input type="submit" value="Přidat">
</form>
</section>