<?php
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ($_SERVER["REQUEST_METHOD"]==="GET") {
    $statement = $conn->prepare("delete from stav_objednavek where idSTAV = ?");
    $statement->execute([$_GET["idStav"]]);
}
?>
