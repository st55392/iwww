<?php
$conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if($_SERVER["REQUEST_METHOD"]==="POST"){
    $statement = $conn->prepare("update stav_objednavek set stav=? where idSTAV = ?");
    $statement->execute([$_POST["stav"],$_GET["idStav"]]);
    header("Location: index.php?page=stavy_objednavek/stavy_objednavek");
}else if ($_SERVER["REQUEST_METHOD"]==="GET"){
    $statement = $conn->prepare("select * from stav_objednavek where idSTAV = ?");
    $statement->execute([$_GET["idStav"]]);
    $data = $statement->fetch(PDO::FETCH_ASSOC);
    echo'<form method = "post">
        <label for="stav">Stav: </label>
        <input type="text" name="stav" value="'.$data["stav"].'">
        <input type="submit" value="Upravit">
        </form>';
}

?>
