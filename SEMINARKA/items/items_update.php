<?php
try {
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $statementProdukt = $conn->prepare("select * from produkty where idPRODUKT = ?");
        $statementProdukt->execute([$_GET['id']]);
        $dataProdukt = $statementProdukt->fetch(PDO::FETCH_ASSOC);

        $statementVyrobce = $conn->prepare("select * from vyrobci where idVYROBCE = ?");
        $statementVyrobce->execute([$dataProdukt['idVYROBCE']]);
        $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);

        echo '<h1>Upravení výrobku</h1>
        <form method="post" enctype="multipart/form-data">
            <label for="fileImg">Obrázek: </label>
            <input type="file" name="fileImg">
            <br>
            <label for="nazev">Nazev: </label>
            <input type="text" name="nazev" id="nazev" required="true" value = "' . $dataProdukt['nazev'] . '">
            <br>
            <label for="popis">Popis: </label>
            <input type="text" name="popis" id="popis" required="true" value = "' . $dataProdukt['popis'] . '">
            <br>
            <label for="cena">Cena: </label>
            <input type="number" name="cena" id="cena" required="true" value = "' . $dataProdukt['cena'] . '">
            <br>
            <label for ="stupnivost">Stupnivost: </label>
            <input type="number" name ="stupnivost" id = "stupnivost" required = "true" value = "'.$dataProdukt['stupnivost'].'">
            <br>
            <label for ="druh">Druh: </label>
            <input type ="text" name = "druh" id ="druh" required="true" value = "'.$dataProdukt['druh'].'">
            <br>
            <label for ="alkohol">Obsah alkoholu: </label>
            <input type ="number" step="0.1" name="alkohol" id="alkoho" required="true" value = "'.$dataProdukt['alkohol'].'">
            <br>
            <label for="vyrobce">Výrobce: </label>
            <select name="vyrobce" value = "' . $dataVyrobce['nazev'] . '">';
        $data = $conn->query("select * from vyrobci")->fetchAll();
        foreach ($data as $row) {
            echo '<option>' . $row['nazev'] . '</option>';
        }

        echo '</select>
            <a href="?page=/producers/producers">Upravit výrobce</a>
            <br>
            <input type="submit" name="submit" value="ODESLAT">
        </form>';

    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $statementVyrobce = $conn->prepare("select * from vyrobci where nazev = ?");
        $statementVyrobce->execute([$_POST['vyrobce']]);
        $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);
        if ($_FILES['fileImg']['size'] == 0) {
            $statementUpdate = $conn->prepare("update produkty set nazev = ?, popis = ?, cena = ?,stupnivost=?,druh=?,alkohol=?, idVYROBCE = ? where idPRODUKT = ?");
            $statementUpdate->execute([$_POST['nazev'], $_POST['popis'], $_POST['cena'],$_POST["stupnivost"],$_POST["druh"],$_POST["alkohol"], $dataVyrobce['idVYROBCE'], $_GET['id']]);
        } else {
            move_uploaded_file($_FILES['fileImg']['tmp_name'], "uploads/" . $_FILES['fileImg']['name']);
            $fp = fopen(realpath("uploads/" . $_FILES['fileImg']['name']), 'rb');
            $statementUpdate = $conn->prepare("update produkty set nazev = :nazev, popis = :popis, cena = :cena,stupnivost=:stupnivost,druh=:druh,alkohol=:alkohol, idVYROBCE = :idVyr, obrazek = :obr, nazev_obrazek = :obrNaz, typ_obrazek = :obrTyp where idPRODUKT = :idProd");
            $statementUpdate->bindParam(":nazev", $_POST['nazev']);
            $statementUpdate->bindParam(":popis", $_POST['popis']);
            $statementUpdate->bindParam(":cena", $_POST['cena']);
            $statementUpdate->bindParam(":idVyr", $dataVyrobce['idVYROBCE']);
            $statementUpdate->bindParam(":stupnivost",$_POST["stupnivost"]);
            $statementUpdate->bindParam(":druh",$_POST["druh"]);
            $statementUpdate->bindParam(":alkohol",$_POST["alkohol"]);
            $statementUpdate->bindParam(":obr", $fp, PDO::PARAM_LOB);
            $statementUpdate->bindParam(":obrNaz", $_FILES['fileImg']['name']);
            $statementUpdate->bindParam(":obrTyp", $_FILES['fileImg']['type']);
            $statementUpdate->bindParam(":idProd", $_GET['id']);
            $statementUpdate->execute();
        }
        header("Location: index.php?page=items/items");
    }
} catch (PDOException $exception) {
    echo $exception;
}

?>

