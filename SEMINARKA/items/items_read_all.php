<h1>Výrobky</h1>
<?php
try {
    echo "<a href = \"? page=/items/items&action=new\">Přidat výrobek</a><p></p>";
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = $conn->query("select * from produkty")->fetchAll();
    echo "<table style='width: 100%' border='1'>
            <tr>
                <th>ID</th>
                <th>Název</th>
                <th>Popis</th>
                <th>Cena</th>
                <th>Stupnivost</th>
                <th>Druh</th>
                <th>Alkohol</th>
                <th>Výrobce</th>
                <th>U</th><th>S</th></tr>";
    foreach($data as $row){
        $statementVyrobce = $conn->prepare("select * from vyrobci where idVYROBCE = ?");
        $statementVyrobce->execute([$row["idVYROBCE"]]);
        $dataVyrobce = $statementVyrobce->fetch(PDO::FETCH_ASSOC);
        echo'<tr>
            <td>'.$row["idPRODUKT"].'</td>
            <td>'.$row["nazev"].'</td>
            <td>'.$row["popis"].'</td>
            <td>'.$row["cena"].' Kč</td>
            <td>'.$row["stupnivost"].'°</td>
            <td>'.$row["druh"].'</td>
            <td>'.$row["alkohol"].' %</td>
            <td>'.$dataVyrobce['nazev'].'</td>
            <td><a href="?page=items/items&action=update&id='.$row["idPRODUKT"].'">Upravit</a></td>
            <td><a href="?page=items/items&action=delete&id='.$row["idPRODUKT"].'">Smazat</td>
            </tr>';
    }
    echo'</table>';
}
catch(PDOException $ex){
    echo 'Nepodařilo se načíst výrobky';
}
?>
<p></p>
