<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    try {
        $statement = $conn->prepare("insert into produkty(nazev,popis,cena,idVYROBCE,obrazek,nazev_obrazek,typ_obrazek,stupnivost,druh,alkohol) 
            values (:nazev,:popis,:cena,:vyrobce,:obrazek,:nazevObr,:typObr,:stupnivost,:druh,:alkohol)");
        move_uploaded_file($_FILES['fileImg']['tmp_name'],"uploads/".$_FILES['fileImg']['name']);
        $fp = fopen(realpath("uploads/".$_FILES['fileImg']['name']),'rb');
        $statement->bindParam(":nazev", $_POST['nazev']);
        $statement->bindParam(":popis", $_POST['popis']);
        $statement->bindParam(":cena", $_POST['cena']);
        $statVyr = $conn->prepare("select idVYROBCE from vyrobci where nazev = :nazev");
        $statVyr->bindParam(":nazev",$_POST['vyrobce']);
        $statVyr->execute();
        $dataVyrobce= $statVyr->fetch(PDO::FETCH_ASSOC);
        $statement->bindParam(":vyrobce", $dataVyrobce['idVYROBCE']);
        $statement->bindParam(":obrazek", $fp,PDO::PARAM_LOB);
        $statement->bindParam(":nazevObr",$_FILES['fileImg']['name']);
        $statement->bindParam(":typObr",$_FILES['fileImg']['type']);
        $statement->bindParam(":stupnivost",$_POST["stupnivost"]);
        $statement->bindParam(":druh",$_POST["druh"]);
        $statement->bindParam(":alkohol",$_POST["alkohol"]);
        $statement->execute();

        header("Location: index.php?page=items/items");
    } catch (PDOException $exception) {
        echo $exception;
    }
}
?>
<section class="form">
<h1>Vytvoření výrobku</h1>
<form method="post" enctype="multipart/form-data">
    <label for="fileImg">Obrázek: </label>
    <input type="file" name="fileImg" required="true" accept="image/*">
    <br>
    <label for="nazev">Nazev: </label>
    <input type="text" name="nazev" id="nazev" required="true">
    <br>
    <label for="popis">Popis: </label>
    <input type="text" name="popis" id="popis" required="true">
    <br>
    <label for="cena">Cena: </label>
    <input type="number" name="cena" id="cena" required="true">
    <br>
    <label for ="stupnivost">Stupnivost: </label>
    <input type="number" name ="stupnivost" id = "stupnivost" required = "true">
    <br>
    <label for ="druh">Druh: </label>
    <input type ="text" name = "druh" id ="druh" required="true">
    <br>
    <label for ="alkohol">Obsah alkoholu: </label>
    <input type ="number" step="0.1" name="alkohol" id="alkohol" required="true">
    <br>
    <label for="vyrobce">Výrobce: </label>
    <select name="vyrobce">
        <?php
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $data = $conn->query("select * from vyrobci")->fetchAll();
        foreach ($data as $row){
            echo '<option>'.$row['nazev'].'</option>';
        }
        ?>

    </select>
    <br>
    <input type="submit" name="submit" value="ODESLAT">
</form>
</section>