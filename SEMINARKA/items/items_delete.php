<?php
if ($_SERVER["REQUEST_METHOD"] === "GET") {
    try {
        $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statementDelete = $conn->prepare("delete from produkty where idPRODUKT = ?");
        $statementDelete->execute([$_GET['id']]);
        header("Location: index.php?page=items/items");
    }catch (PDOException $exception){
        echo $exception;
        echo 'Nepodařilo se odstranit';
    }
}
?>
