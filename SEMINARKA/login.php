<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if(isset($_POST['login'])) {
        $message = "";
        try {
            $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $conn->prepare("select * from uzivatele where email = :email and heslo = :password and registrovany = :reg");
            $reg = 1;
            $statement->bindParam(":reg",$reg);
            $statement->bindParam(':email', $_POST["email"]);
            $statement->bindParam(':password', $_POST["password"]);
            $statement->execute();
            $data = $statement->fetch(PDO::FETCH_ASSOC);
            if(!empty($data)) {
                $message = "Přihlášen";
                $_SESSION ["id"] = $data["idUZIVATEL"];
                $_SESSION["admin"] = $data["admin"];
                unset($_SESSION["cart"]);
                $statement = $conn->prepare("select idKOSIK from kosiky where idUZIVATELE = ?");
                $statement->execute([$data["idUZIVATEL"]]);
                $dataKos = $statement->fetch(PDO::FETCH_ASSOC);
                if(!empty($dataKos)){
                    $statement = $conn->prepare("select * from produkty_kosiku where idKOSIK = ?");
                    $statement->execute([$dataKos["idKOSIK"]]);
                    $dataProduktyKos = $statement->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($dataProduktyKos as $row){
                        $_SESSION['cart'][$row['idPRODUKT']];
                        $_SESSION['cart'][$row['idPRODUKT']]['quantity'] = $row["pocetKs"];
                    }
                }
                header('Location:' . BASE_URL);
            }else{
                $message = "Uzivatel nenalazen";
            }
        } catch (PDOException $ex) {
            echo $ex;
            $message = "Neprihlasen";
        }
        echo $message;
    }
}
?>
<section class="form">
    <h1>Přihlášení</h1>
    <form method="post">
        Email:
        <br>
        <input type="email" name="email">
        <br>
        Heslo<br>
        <input type="password" name="password">
        <br>
        <input type="submit" name="login" value="Přihlásit"><br>
        <p></p>
        Nejste u nás registrovaní?<br>
        <div class="card"><a href = "?page=users/users&action=new">Registrovat</a></div><p></p>
        <p></p>
    </form>
</section>
