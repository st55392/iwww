<div class="center-wrapper">
    <h1>Kdo jsme a proč tu jsme?</h1>
    <p>
        Jsme inovace v konzumaci piva! Máte chuť na pivo a nemůžete ho nikde sehnat? Nechce se vám do hospody a trávit
        hromadu času v obchodech? Tak právě pro Vás jsme tu.
    </p>
    <p>
        U nás naleznete téměř všechny piva prodávané v České republice. Dodáváme do 2 dnů od objednání.
        Dokážeme Vám poradit, jaké pivo je pro Vás vhodné. Neváhejte a prolistujte si náš katalog a v případě
        jakýchkoliv dotazů nás neváhejte kontaktovat!
    </p>
    <hr>
</div>
<div class="center-wrapper">
    <h1>U nás nejdete piva z těchto pivovarů:</h1>
    <div class="flex-wrap">
        <div class="card">
            <img src="./img/raven.jpg" alt="logo zakaznika1">
            <h2>Pivovar Raven</h2>
            <p>
                Poměrně mladý malý pivovar z Plzně. Byl založen v roce 2015 a vaří převážně pivní speciály.
                Vaří piva typu: APA, IPA, Stout. Uvařili první českou White IPU, českého kyseláče (Pilsener Wiesse).
                Je to malý soukromý pivovar, který si ale ovšem mezi českými pivaři vybudoval velkou oblibu.
            </p>
        </div>

        <div class="card">
            <img src="./img/krakonos.png" alt="logo zakaznika2">
            <h2>Pivovar Krakonoš</h2>
            <p>
                Jedná se o menší Český pivovar založený v roce 1582 v Trutnově. Leží v centru města.
                Vyrábí 5 druhů piv (světlé výčetní - desítka, tmavé výčepní - desítka, světlý ležák - jedenáctka,
                světlý ležák - dvanáctka a speciální (velikonoční/vánoční) čtrnáctka).
            </p>
        </div>
        <div class="card">
            <img src="./img/pilsner.png" alt="logo zakaznika3">
            <h2>Pivovar Pilsner Urquell</h2>
            <p>
                Český pivovar založený v roce 1842 v Plzni, produkující pivo plzeňského typu. Pod tento pivovar v
                současné
                době
                patří i pivovary Radegast a Velké Popovice. Pilsner Urqell je největším výrobcem piva v Česku
                a také největším exportérem piva do zahraničí.
            </p>
        </div>
    </div>
    <hr>
</div>
<div class="center-wrapper">
    <h1>Další oblíbené pivovary:</h1>
    <div class="flex-wrap">

        <div class="card">
            <img src="./img/beer_poets.png" alt="beer poets">
            <h2>Pivovar Beer Poets</h2>
        </div>

        <div class="card">
            <img src="./img/krusovice.png" alt="Krušovice">
            <h2>Pivovar Krušovice</h2>
        </div>

        <div class="card">
            <img src="./img/brezno.png" alt="Březňák">
            <h2>Pivovar Velké Březno</h2>
        </div>
    </div>
</div>
