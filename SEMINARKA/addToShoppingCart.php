<?php
$message = '';
include "config.php";
try {
    $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($_SERVER["REQUEST_METHOD"] === "GET") {
        if ($_GET["action"] == "add" || $_GET["action"]=="plus") {
            if (array_key_exists($_GET["id"], $_SESSION['cart'])) {
                $_SESSION['cart'][$_GET['id']]['quantity']++;
            } else {
                $_SESSION['cart'][$_GET['id']];
                $_SESSION['cart'][$_GET['id']]['quantity'] = 1;
            }
            header("Location: index.php?page=shopping_cart");
        }else if ($_GET["action"] == "minus") {
            if (array_key_exists($_GET["id"], $_SESSION['cart'])) {
                $_SESSION['cart'][$_GET['id']]['quantity']--;
                if ($_SESSION['cart'][$_GET['id']]['quantity'] == 0) {
                    unset($_SESSION['cart'][$_GET['id']]);
                }
            }
            header("Location: index.php?page=shopping_cart");
        } else if ($_GET["action"] == "del") {
            unset($_SESSION['cart'][$_GET['id']]);
            header("Location: index.php?page=shopping_cart");
        }
    }
} catch (PDOException $exception) {
    echo $exception;
}
?>
